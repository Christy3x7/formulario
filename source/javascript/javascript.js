// var phone = document.getElementById("phone");
// phone.addEventListener("input", function () {
//   if (this.value.length > 11) this.value = this.value.slice(0, 11);
// });

function feedback(message) {
  // $(".business").hide();
  // $(".thanks").fadeIn();
  // $(".box-button").addClass("active");
  window.location.href = './gracias.html'
  $(".question").addClass("exito");
}

var placeholder =
  "Ej: \n ¿Qué quieres desarrollar? \n ¿Quieres cambiar un sistema ya implementado? \n ¿Cuáles son tus objetivos con el desarrollo? \n ¿Tienes fecha límite? \n Cualquier dato o información que consideres relevante.\n\n";
$("textarea").attr("placeholder", placeholder);

// validacion

$.validator.setDefaults({
  submitHandler: function (form) {
    // Default to company form
    var formSpringId = "b9adfb06-8127-4b09-bce6-86c867eb85e5";
    if ($(form).attr("id") !== "company")
      formSpringId = "764c8080-b1ab-454f-8c1a-850da49cf848";

    var submitForm = function () {
      var data = $(form).serializeArray();

      $.post("https://aureolab.cl/cotizador/mailer.php", data).done(function (
        response
      ) {
        if (response.success) {
          // feedback("¡Solicitud enviada con éxito!");
          feedback();
        } else {
          feedback("Error :(");
        }
      });
    };

    __ss_noform.push(["submit", submitForm, formSpringId]);
  },
});

$(function () {
  $("#company").validate({
    errorPlacement: function (error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']")
        .append(error);
      $(".complete").show();
    },
    rules: {
      nombre: "required",
      phone: "required",
      empresa: "required",
      industria: "required",
      detalles: "required",
      nombre: {
        required: true,
        minlength: 2,
      },
      phone: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      detalles: {
        required: true,
        minlength: 2,
      },
    },
    messages: {
      nombre: {
        required: "",
        minlength: "",
      },
      phone: "",
      email: "",
      empresa: "",
      industria: "",
      employees: "",
      detalles: "",
      presupuesto: "",
      servicio: "",
    },
  });

  $("#person").validate({
    errorPlacement: function (error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']")
        .append(error);
      $(".complete").show();
    },
    rules: {
      nombre: "required",
      phone: "required",
      detalles: "required",
      nombre: {
        required: true,
        minlength: 2,
      },
      phone: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      detalles: {
        required: true,
        minlength: 2,
      },
    },
    messages: {
      nombre: {
        required: "",
        minlength: "",
      },
      phone: "",
      email: "",
      detalles: "",
      presupuesto: "",
      etapa: "",
      servicioPersona:"",
    },
  });

  $("#industria").on("change", function () {
    if ($(this).val() === "Otros") {
      $("#other-industry-field").removeClass("hidden");
    }
    else{
      $("#other-industry-field").addClass("hidden");
    }
  });

  $("#etapa").on("change", function () {
    if ($(this).val() === "Otro") {
      $("#otraEtapa").removeClass("hidden");
    }
    else{
      $("#otraEtapa").addClass("hidden");
    }
  });

  $("#semejanza").on("change", function () {
    if ($(this).val() === "Otro") {
      $("#otraOpcion").removeClass("hidden");
    }
    else{
      $("#otraOpcion").addClass("hidden");
    }
  });

  $("#medio").on("change", function () {
    if ($(this).val() === "Otro") {
      $("#otroMedio").removeClass("hidden");
    }
    else{
      $("#otroMedio").addClass("hidden");
    }
  });
});
